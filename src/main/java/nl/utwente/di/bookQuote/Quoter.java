package nl.utwente.di.bookQuote;

import java.util.HashMap;

class Quoter {

    private HashMap<String, Double> values = new HashMap<String, Double>();


    double getBookPrice(String isbn) {
        values.put("1", 10.0);
        values.put("2", 45.0);
        values.put("3", 20.0);
        values.put("4", 10.0);
        values.put("5", 10.0);
        if (!values.containsKey(isbn)) {
            return 0.0;
        }
        return values.get(isbn);


        }
    }
